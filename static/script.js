function validateCHCAddress(id)
{
  var address_regex = /^[2Cc][a-zA-Z1-9]{25,43}$/;
  if(!address_regex.test($("#"+id).val()))
  {
    var input = $("#"+id);
    $("#glypcn"+id).remove();
    input.addClass("text-danger");
    return false;
  }
  else{
    var input = $("#"+id);
    input.removeClass("text-danger");
    $("#glypcn"+id).remove();
    return true;
  }
  
}

$(document).ready(
  function()
  {
    $("#submit").click(function()
    {
      if(!validateCHCAddress("address"))
      {
        return false;
      }
      $("form#withdrawForm").submit();
    }
    
    );
  }
  
  
  );