const fs = require('fs');

/* Configuration File */
const config = require("./config.json");

/* Chaincoin Stuff */
const chaincoin = require('node-chaincoin');

const chaincoin_client = new chaincoin.Client(config.daemon);

/* Website Stuff */
const express = require('express');
const web_app = express();

web_app.use('/static', express.static('./static/'));

web_app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

web_app.get('/fetch-address/', (req, res) => {
    getFreshAccount(fresh_account => {
        res.json(fresh_account);
    });
});

web_app.listen(3000);

function getFreshAccount(then) {
    chaincoin_client.cmd('listreceivedbyaddress', 1, true, (err, address_array) => {
        if (err) return console.error(err);
        for (var i in address_array) {
            var account = address_array[i];
            // we want accounts unused this round. allow small amounts of change.
            if (account.label === 'deposit' && account.amount < 1) {
                then(account)
                return;
            }
        }
        // if no empty / change accounts are found, we make a new one
        chaincoin_client.cmd('getnewaddress', 'deposit', (err, fresh_account) => {
            if (err) return console.error(err);
            getFreshAccount(then);
        })
    });
}